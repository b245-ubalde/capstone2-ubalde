const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./Routes/userRoutes.js");
const productRoutes = require("./Routes/productRoutes.js");
const orderRoutes = require("./Routes/orderRoutes.js");

const port = 4005;

const app = express();



	mongoose.set('strictQuery', true);
	// MongoDB Connection
	mongoose.connect("mongodb+srv://admin:admin@batch245-ubalde.4valdm5.mongodb.net/batch245_Capstone2API_Ubalde?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	})


	let db = mongoose.connection;

	// For Error Handling
	db.on("error", console.error.bind(console, "Connection Error!"));

	// For Validation of the Connection
	db.once("open", () => {console.log("We're on the cloud!")});


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


// For Routing
app.use("/user", userRoutes);
app.use("/products", productRoutes);
app.use("/order", orderRoutes);


app.listen(port, () => console.log(`Server is currently running at port ${port}. Let's G!`));

// model >> controller >> routes >> server