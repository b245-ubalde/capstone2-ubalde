const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "userId is required!"]
	},
	productId: {
		type: String,
		required: [true, "productId is required!"]
	},
	quantity: {
		type: Number,
		required: 1
	},
	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required!"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})


module.exports = mongoose.model("Order", orderSchema);