/*
	Routes:
		- User registration - DONE
		- User authentication - DONE
		- Create Product (Admin) - DONE
		- Retrieve all active products - DONE
		- Retrieve single product - DONE
		- Update Product Information (Admin) - DONE
		- archive Product (Admin) - DONE
		- Non-admin User checkout (Create Order)
		- Retrieve User Details



	Data Models:

	OPTION 1
	User {
		email: String,
		password: String,
		isAdmin: Boolean, - default to false
		orders:[
			{
				products: [
					{
						productName: String,
						quantity: Number
					}
				],
				totalAmount: number,
				purchasedOn: Date
			}
		]
	}


	Product: {
		name: String,
		description: String,
		price: Number,
		isActive: Boolean,  default to true
		createdOn: Date,
		orders:[
			{
				orderId: String
			}
		]
	}



	OPTION2:

	User {
		email: String,
		password: string,
		isAdmin: Boolean
	}

	Product {
		name: String,
		description: String,
		price: Number,
		isActive: Boolean - default: true
		createdOn: Date - new Date()
	}

	Order {
		userId: objectId
		products: [
			{
				productId: String,
				quantity: Number
			}
		]
		totalAmount: Number
		purchasedOn: Date - new Date()
	}
*/