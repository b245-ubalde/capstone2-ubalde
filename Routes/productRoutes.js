const express = require("express");
const router = express.Router();
const productController = require("../Controllers/productController.js");
const auth = require("../auth.js");




// Routes
router.post("/addProduct", auth.verify, productController.addProduct);

router.get("/showActiveProducts", productController.showActiveProducts);

router.get("/showAllProducts", productController.showAllProducts);


// Routes with Params
router.get("/showProduct/:productId", productController.productDetails);

router.put("/updateProduct/:productId", auth.verify, productController.updateProduct);

router.put("/archiveProduct/:productId", auth.verify, productController.archiveProduct);

router.put("/reactivateProduct/:productId", auth.verify, productController.reactivateProduct);

module.exports = router;