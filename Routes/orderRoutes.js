const express = require("express");
const router = express.Router();
const orderController = require("../Controllers/orderController.js");
const auth = require("../auth.js");


// Routes

// Create Order
router.post("/createOrder", auth.verify, orderController.createOrder);

// View All Orders
router.get("/viewAllOrders", auth.verify, orderController.retrieveAllOrders);

// View My Orders
router.get("/viewMyOrders", auth.verify, orderController.viewMyOrders);

module.exports = router;