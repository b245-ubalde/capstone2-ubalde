const express = require("express");
const router = express.Router();
const userController = require("../Controllers/userController.js");
const auth = require("../auth.js");



// Routes

// Route for registration
router.post("/register", userController.userRegistration);

// Route for User Authentication
router.post("/login", userController.userAuthentication);


// Route for Retrieving User Details
router.get("/details", auth.verify, userController.userDetails);



// Routes with Params

// Create Order
// router.post("/createOrder/:productId", auth.verify, userController.createOrder);

module.exports = router;