// const express = require("express");
const mongoose = require("mongoose");
const Product = require("../Models/productSchema.js");
const auth = require("../auth.js");


// Controllers

// Add Product
module.exports.addProduct = (request, response) => {
	const adminData = auth.decrypt(request.headers.authorization);

	// console.log(adminData);

	if(adminData.isAdmin == true){
		const input = request.body;

		let newProduct = new Product({
			name: input.name,
			description: input.description,
			price: input.price,	
			image: input.image
		});


		// Save to Database
		return newProduct.save()
		.then(product => {
			// console.log(product);
			response.send(product); //Product successfully added
		})
		.catch(error => {
			// console.log(error);
			response.send(error);
		})
	}
	else{
		return response.send("User is not admin"); // User is not an admin/ Invalid user
	}
}




// Display All Products
module.exports.showActiveProducts = (request, response) => {
	Product.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}


// Display All Products
module.exports.showAllProducts = (request, response) => {
	Product.find({})
	.then(result => response.send(result))
	.catch(error => response.send(error));
}


// Retrieve single product
module.exports.productDetails = (request, response) => {
	const productId = request.params.productId;

	Product.findById(productId)
	.then(result => {
		if(result === null){
			return response.send(false) //Invalid Product ID
		}
		else{
			return response.send(result)
		}
	})
	.catch(error => response.send(false));
}

// Update Product
module.exports.updateProduct = (request, response) => {
	const userData = auth.decrypt(request.headers.authorization);

	const productId = request.params.productId;

	const input = request.body;

	if(userData.isAdmin !== true){
		return response.send(false); //User is not an admin
	}
	else{
		Product.findOne({_id: productId})
		.then(result => {
			if(result === null){
				return response.send(false); //Invalid Product ID/ No matching Product ID
			}
			else{
				let updatedProduct = {
					name: input.name,
					description: input.description,
					price: input.price,
					image: input.image
				}

				Product.findByIdAndUpdate(productId, updatedProduct, {new: true})
				.then(result => {
					return response.send(true) //Updated Product
				})
				.catch(error => {response.send(false)});
			}
		})
		.catch(error => { response.send(false)});
	}
}



// Archive Product
module.exports.archiveProduct = (request, response) => {
	const userData = auth.decrypt(request.headers.authorization);
	const productId = request.params.productId;
	const input = request.body;

	if(userData.isAdmin !== true){
		return response.send(false); //Invalid User / User is not admin
	}
	else{
		Product.findOne({_id: productId})
		.then(result => {
			if(result === null){
				return response.send(false); //Invalid Product ID/ No Matching Product ID
			}
			else{

				let archivedProduct = {
					isActive: input.isActive
				}

				// console.log(archivedProduct);
				Product.findByIdAndUpdate(productId, archivedProduct, {new: true})
				.then(result => {
					return response.send(true) //Updated Product
				})
				.catch(error => {
					return response.send(false);
				})
			}
		})
		.catch(error => {
			return response.send(false);
		})
	}
}


// Reactivate Product

module.exports.reactivateProduct = (request, response) => {
	const userData = auth.decrypt(request.headers.authorization);
	const productId = request.params.productId;
	const input = request.body;

	if(userData.isAdmin !== true){
		return response.send(false); //User is not admin
	}
	else{
		Product.findOne({_id: productId})
		.then(result => {
			if(result === null){
				return response.send(false); //Invalid Product ID
			}
			else{
				let reactivatedProduct = {
					isActive: input.isActive
				}

				Product.findByIdAndUpdate(productId, reactivatedProduct, {new: true})
				.then(result => {
					return response.send(true) //Product hs been Reactivated
				})
				.catch(error => {
					return response.send(false);
				})
			}
		})
		.catch(error => {
			return response.send(false);
		})
	}
}