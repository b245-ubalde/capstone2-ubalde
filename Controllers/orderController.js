const mongoose = require("mongoose");
const Order = require("../Models/orderSchema.js");
const Product = require("../Models/productSchema.js");
const User = require("../Models/userSchema.js");
const auth = require("../auth.js");


// Create Order
module.exports.createOrder = (request, response) => {
	const userData = auth.decrypt(request.headers.authorization);

	const input = request.body;


	// Verify product ID
	Product.findById(input._id)
	.then(result => {
		if(result === null){
			return response.send(false) //Invalid Product ID / No Matching Product ID
		}
		else{

			if(userData.isAdmin === true){
				return response.send(false) // Admins cannot make purchases
			}
			else{
				let productName = result.name;
				let productPrice = result.price;

				let total = productPrice * input.quantity;


				let newOrder = new Order({
					userId: userData._id,
					productName: input.name,
					productId: input._id,
					quantity: input.quantity,
					totalAmount: total
				})

				// console.log(newOrder);

				// Save in Database
				newOrder.save()
				.then(save => {
					return response.send(result) //Order successful
				})
				.catch(error => {
					return response.send(false)
				})
			}
		}
	})
	.catch(error => {
		return response.send(false);
	})
}


// Retrieve All Orders
module.exports.retrieveAllOrders = (request, response) => {
	const adminData = auth.decrypt(request.headers.authorization);

	if(adminData.isAdmin === false){
		return response.send(error) // User is not an admin
	}
	else{
		Order.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
}


// View my Orders
module.exports.viewMyOrders = (request, response) => {
	const userData = auth.decrypt(request.headers.authorization);
	// let userId = request.params.userId

	User.findOne({_id: userData._id})
	if(userData.isAdmin === true){
		return response.send(false) // Admins cannot purchase orders, no orders list for Admins
	}
	else{
		Order.find({userId: userData._id})
		.then(result => {
			return response.send(result) // Show orders list for a specific user
		})
		.catch(error => response.send(false));
	}

}