const express = require("express");
const mongoose = require("mongoose");
const User = require("../Models/userSchema.js");
const Product = require("../Models/productSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


// Controllers

// User Registration
module.exports.userRegistration = (request, response) => {
	const input = request.body;

	User.findOne({email: input.email})
	.then(result => {
		if(result !== null){
			return response.send(false); //Email is already taken/registered
		}
		else{
			let newUser = new User({
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
				isAdmin: input.isAdmin,
				orders: input.orders
			})


			// To save in database
			newUser.save()
			.then(save => {
				return response.send(true) //Account is successfully registered
			})
			.catch(error => {
				return response.send(false)
			})
		}
	})
	.catch(error => {
		return response.send(false);
	})
}


// User Authentication (Login)
module.exports.userAuthentication = (request, response) => {
	let input = request.body;

	User.findOne({email: input.email})
	.then(result => {
		if(result === null){
			return response.send(false); // Email is not yet registered
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)}) //Login successful
			}
			else{
				return response.send(false); //Password is incorrect
			}
		}
	})
	.catch(error => {
		return response.send(false); // Error
	})
}


// Retrieve User Details
module.exports.userDetails = (request, response) => {
	const userData = auth.decrypt(request.headers.authorization);

	return User.findById(userData._id)
	.then(result => {
		result.password = " ";

		console.log(result);
		return response.send(result);
	})
}