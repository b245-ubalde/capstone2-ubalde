const jwt = require("jsonwebtoken");

const privateKey = "Capstone2API";

module.exports.createAccessToken = (user) => {
	const data = {
		_id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, privateKey, {});
}


// Token Verification
module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;

	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		// console.log(token);

		return jwt.verify(token, privateKey, (err, data) => {
			if(err){
				return response.send({auth: "Failed!"})
			}
			else{
				next();
			}
		})
	}
	else{
		return response.send({auth: "Failed!"})
	}
}



// Token Decryption
module.exports.decrypt = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7,token.length);
		return jwt.verify(token, privateKey, (err, data) => {
			if(err){
				return null;
			}
			else{
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else{
		return null;
	}
}